package cpu

func load8(cpu CPU, register register) {
	incrementPC(cpu)
	value := cpu.GetByte(cpu.GetProgramCounter())
	cpu.SetRegisterValue(register, value)
	incrementPC(cpu)
}

func load16(cpu CPU, registerLo register, registerHi register) {
	incrementPC(cpu)
	valueLo := cpu.GetByte(cpu.GetProgramCounter())
	incrementPC(cpu)
	valueHi := cpu.GetByte(cpu.GetProgramCounter())
	cpu.SetRegisterValue(registerLo, valueLo)
	cpu.SetRegisterValue(registerHi, valueHi)
	incrementPC(cpu)
}

// Load a 16 bit pointer's value into a single 8 bit register
func load16Ptr8(cpu CPU, registerLo register, registerHi register, targetRegister register) {
	addr16 := Get16BitFrom8(cpu.GetRegisterValue(registerLo), cpu.GetRegisterValue(registerHi))
	val := cpu.GetByte(addr16)
	cpu.SetRegisterValue(targetRegister, val)
	incrementPC(cpu)
}

func loadSP(cpu CPU) {
	incrementPC(cpu)
	valueLo := cpu.GetByte(cpu.GetProgramCounter())
	incrementPC(cpu)
	valueHi := cpu.GetByte(cpu.GetProgramCounter())
	cpu.SetStackPointer(Get16BitFrom8(valueHi, valueLo))
	incrementPC(cpu)
}
