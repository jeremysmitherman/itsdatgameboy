package cpu

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGameboy_Step(t *testing.T) {
	gb := NewGameboy()
	rom := [0xFFFF]uint8 {
		0xD3,
		0x00,
		0x01, 0x01, 0x00,
		0x02,
		0x03,
		0x04,
		0x05,
		0x06, 0xFF,
		0x07,
	}

	rom[0x100] = 0x05
	gb.LoadROM(rom)

	// 0xD3 Unused instruction, checking for error raised on unidentified opcode
	err := gb.Step()
	assert.NotNil(t, err)

	// 0x00 NOP
	gb.Step()
	assert.Equal(t, uint16(2), gb.programCounter)

	// 0x01 LD BC, d16
	gb.Step()
	assert.Equal(t, uint16(256), Get16BitFrom8(gb.registers[REGISTER_B], gb.registers[REGISTER_C]))

	// 0x02 LD (BC), A
	gb.SetRegisterValue(REGISTER_B, 0x01)
	gb.SetRegisterValue(REGISTER_C, 0x00)
	gb.Step()
	assert.Equal(t, uint8(5), gb.registers[REGISTER_A])

	// 0x03 INC BC
	gb.SetRegisterValue(REGISTER_B, 0x00)
	gb.SetRegisterValue(REGISTER_C, 0xFF)
	gb.Step()
	assert.Equal(t, uint8(0x01), gb.GetRegisterValue(REGISTER_B))
	assert.Equal(t, uint8(0x00), gb.GetRegisterValue(REGISTER_C))

	// 0x04 INC B
	gb.SetRegisterValue(REGISTER_B, 0x0F)
	gb.Step()
	assert.Equal(t, uint8(0x10), gb.GetRegisterValue(REGISTER_B))
	assert.True(t, gb.flagRegisters.halfCarry)

	// 0x05 DEC B
	gb.SetRegisterValue(REGISTER_B, 0x10)
	gb.ClearFlag(FLAG_HALFCARRY)
	gb.Step()
	assert.Equal(t, uint8(0xF), gb.GetRegisterValue(REGISTER_B))
	assert.True(t, gb.flagRegisters.halfCarry)
	assert.True(t, gb.flagRegisters.subtract)

	// 0x06 LD B, d8
	gb.Step()
	assert.Equal(t, uint8(0xFF), gb.registers[REGISTER_B])

	// 0x07 RLCA
	gb.SetRegisterValue(REGISTER_A, 0x80)
	gb.ClearFlag(FLAG_CARRY)
	gb.Step()
	assert.Equal(t, uint8(0x01), gb.GetRegisterValue(REGISTER_A))
	assert.True(t, gb.flagRegisters.carry)
}
