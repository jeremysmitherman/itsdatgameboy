package cpu

type CPU interface {
	SetProgramCounter(uint16)
	GetProgramCounter() uint16
	SetStackPointer(uint16)
	GetByte(uint16) uint8
	SetRegisterValue(register, uint8)
	GetRegisterValue(register register) uint8
	SetFlag(flag flag)
	ClearFlag(flag flag)
}

func NOP(cpu CPU) {
	incrementPC(cpu)
}

func incrementPC(cpu CPU) {
	cpu.SetProgramCounter(cpu.GetProgramCounter() + 1)
}

func incrementPCBy(cpu CPU, amount uint16) {
	cpu.SetProgramCounter(cpu.GetProgramCounter() + amount)
}

func Get16BitFrom8(hi uint8, lo uint8) uint16 {
	return uint16(hi) << 8 | uint16(lo)
}

func Get8BitFrom16(val uint16) (uint8, uint8) {
	return uint8(val >> 8), uint8(val)
}
