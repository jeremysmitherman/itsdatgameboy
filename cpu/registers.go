package cpu

type flag int
const (
	FLAG_ZERO flag = iota
	FLAG_SUBTRACT
	FLAG_HALFCARRY
	FLAG_CARRY
)

type register int
const (
	REGISTER_A register = iota
	REGISTER_B
	REGISTER_C
	REGISTER_D
	REGISTER_E
	REGISTER_F
	REGISTER_H
	REGISTER_L
	REGISTER_PC
	REGISTER_SP
)

type flagRegisters struct {
	zero bool
	subtract bool
	halfCarry bool
	carry bool
}
