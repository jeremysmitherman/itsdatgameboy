package cpu

import (
	"errors"
	"fmt"
)

type gameboy struct {
	registers map[register]uint8
	flagRegisters
	programCounter uint16
	stackPointer uint16
	memory [0xFFFF]uint8
}

func (cpu *gameboy) LoadROM(romBytes [0xFFFF]uint8) {
	cpu.memory = romBytes
}

func (cpu *gameboy) Step() error {
	instruction := cpu.memory[cpu.programCounter]
	return cpu.execute(instruction)
}

func (cpu *gameboy) SetProgramCounter(value uint16) {
	cpu.programCounter = value
}

func (cpu *gameboy) GetProgramCounter() uint16 {
	return cpu.programCounter
}

func (cpu *gameboy) GetByte(address uint16) uint8 {
	return cpu.memory[address]
}

func (cpu *gameboy) SetRegisterValue(register register, value uint8) {
	cpu.registers[register] = value
}

func (cpu *gameboy) GetRegisterValue(register register) uint8 {
	return cpu.registers[register]
}

func (cpu *gameboy) SetStackPointer(val uint16) {
	cpu.stackPointer = val
}

func (cpu *gameboy) SetFlag(flag flag) {
	switch flag {
	case FLAG_ZERO:
		cpu.flagRegisters.zero = true
	case FLAG_SUBTRACT:
		cpu.flagRegisters.subtract = true
	case FLAG_HALFCARRY:
		cpu.flagRegisters.halfCarry = true
	case FLAG_CARRY:
		cpu.flagRegisters.carry = true
	}
}

func (cpu *gameboy) ClearFlag(flag flag) {
	switch flag {
	case FLAG_ZERO:
		cpu.flagRegisters.zero = false
	case FLAG_SUBTRACT:
		cpu.flagRegisters.subtract = false
	case FLAG_HALFCARRY:
		cpu.flagRegisters.halfCarry = false
	case FLAG_CARRY:
		cpu.flagRegisters.carry = false
	}
}

func(cpu *gameboy) execute(opcode uint8) error {
	switch opcode {
	case 0x0:
		NOP(cpu)
	case 0x01:
		// LD BC, d16
		load16(cpu, REGISTER_B, REGISTER_C)
	case 0x02:
		// LD (BC), A
		load16Ptr8(cpu, REGISTER_B, REGISTER_C, REGISTER_A)
	case 0x03:
		// INC BC
		incr16(cpu, REGISTER_B, REGISTER_C)
	case 0x04:
		// INC B
		incr8(cpu, REGISTER_B)
	case 0x05:
		// DEC B
		decr8(cpu, REGISTER_B)
	case 0x06:
		// LD B, d8
		load8(cpu, REGISTER_B)
	case 0x07:
		// RCLA
		rot8(cpu, REGISTER_A, false, true)
	case 0x08:
		// LD (a16), SP
		loadSP(cpu)
	default:
		incrementPC(cpu)
		return errors.New(fmt.Sprintf("Undefined instruction: %02x", opcode))
	}
	return nil
}

func NewGameboy() gameboy {
	return gameboy{
		registers:      map[register]uint8{},
		flagRegisters:  flagRegisters{},
		programCounter: 0,
		memory:         [65535]uint8{},
	}
}
