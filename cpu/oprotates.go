package cpu

import "math/bits"

func rot8(cpu CPU, reg register, right bool, withCarry bool) {
	curVal := cpu.GetRegisterValue(reg)
	distance := 1
	if right {
		distance = -distance
	}

	cpu.SetRegisterValue(reg, bits.RotateLeft8(curVal, distance))
	if withCarry {
		if curVal >> 7 == 1 {
			cpu.SetFlag(FLAG_CARRY)
		} else {
			cpu.ClearFlag(FLAG_CARRY)
		}
	}
}
