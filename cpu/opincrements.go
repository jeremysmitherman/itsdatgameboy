package cpu

import (
	"gitlab.com/jeremysmitherman/itsdatgameboy/math"
)

func incr16(cpu CPU, registerHi register, registerLo register) {
	val := Get16BitFrom8(cpu.GetRegisterValue(registerHi), cpu.GetRegisterValue(registerLo))
	val++
	hi, lo := Get8BitFrom16(val)
	cpu.SetRegisterValue(registerHi,  hi)
	cpu.SetRegisterValue(registerLo, lo)
	incrementPC(cpu)
}

func incr8(cpu CPU, register register) {
	newValue, zeroValue, halfCarry := math.U8Incr(cpu.GetRegisterValue(register))
	cpu.SetRegisterValue(register, newValue)
	if zeroValue {
		cpu.SetFlag(FLAG_ZERO)
	}
	if halfCarry {
		cpu.SetFlag(FLAG_HALFCARRY)
	}
	cpu.ClearFlag(FLAG_SUBTRACT)
	incrementPC(cpu)
}

func decr8(cpu CPU, register register) {
	newValue, zeroValue, halfCarry := math.U8Decr(cpu.GetRegisterValue(register))
	cpu.SetRegisterValue(register, newValue)
	if zeroValue {
		cpu.SetFlag(FLAG_ZERO)
	}
	if halfCarry {
		cpu.SetFlag(FLAG_HALFCARRY)
	}
	cpu.SetFlag(FLAG_SUBTRACT)
	incrementPC(cpu)
}
