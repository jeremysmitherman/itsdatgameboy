package math

func U8Incr(a uint8) (newValue uint8, zeroValue bool, halfCarry bool) {
	newValue = a+1
	return newValue, newValue == 0, a < 0x10 && newValue >= 0x10
}

func U8Decr(a uint8) (newValue uint8, zeroValue bool, halfCarry bool) {
	newValue = a-1
	return newValue, newValue == 0, a >= 0x10 && newValue < 0x10
}

func U8Add(a uint8, b uint8) (sum uint8, zeroSum bool, halfCarry bool, carry bool) {
	sum = a + b
	return sum, sum == 0, a < 0x10 && sum >= 0x10, sum < a
}
